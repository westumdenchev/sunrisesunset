
var elem = document.getElementById('draw-shapes');
var params = { width: 1000, height: 500, type: "CanvasRenderer" };
var two = new Two(params).appendTo(elem);

const today = new Date();
const dd = today.getDate();
const mm = today.getMonth(); //January is 0!
const yyyy = today.getFullYear();
const hours=today.getHours();
const minutes=today.getMinutes();
var centerX=params.width/2;
var centerY=params.height/2;
var orbitRadius=180;
var prevMinutes = parseInt(today.getMinutes()); //keeping the previously selected minutes value with default value = the current time

$('#single-input').val(hours +":" + minutes);
$('[data-toggle="datepicker"]').datepicker('setDate', new Date(yyyy, mm, dd));



 //$('.clockpicker').clockpicker();
 var input = $('.clockpicker').clockpicker({
	donetext: 'Done',
		init: function() { 
			console.log("colorpicker initiated");
		},
		afterDone: function() {
			
			two.bind("update", function(frameCount) {
				let ClockpickerHours=document.getElementById("single-input").value.split(":")[0];
				let ClockpickerMinutes=document.getElementById("single-input").value.split(":")[1];
				var degrees1=CalculationDegreesPerMinute(sunrise, sunset , ClockpickerHours, ClockpickerMinutes);
                var diffX=Math.abs(sunGroup.getBoundingClientRect().left - CheckAngle(degrees1).x);
	            var diffY=Math.abs(sunGroup.getBoundingClientRect().top - CheckAngle(degrees1).y) 
				console.log(diffX, diffY);
	            if(diffX<=40 && diffY<=40){
					sunGroup.pause;		
	            } else{		
					sunGroup.rotation += 0.001 * Math.PI;
				}
				prevMinutes = parseInt(ClockpickerMinutes);
				
               }).play();
		}
});

 


elem.addEventListener('click', function() {
		if(two.playing){
			two.pause();
		} else{
			two.play();
		}
	}, false);
	
var sunrise=PrintTime().sunrise;
var sunset=PrintTime().sunset;
console.log(sunrise);
console.log(sunset);
var degrees=CalculationDegreesPerMinute(sunrise, sunset , hours, minutes);  //degrees on page refresh to match current time
var testX= CheckAngle(degrees).x - 500;
var testY = CheckAngle(degrees).y -250;

// two has convenience methods to create shapes.
var topHalf = two.makeRectangle(500, 125, 1000, 250); //top
var sun = two.makeCircle(testX, testY, 35); 		//setting the position of the sun(distance from earth)
var bot = two.makeRectangle(500, 375, 1000, 250); //bottom
var earth = two.makeCircle(centerX, centerY, 75);



// The object returned has many stylable properties:
var earthTexture = new Two.Texture('resource/earth2.png');
earth.fill = earthTexture;
earth.stroke = 'none'; 

var sunTexture = new Two.Texture('resource/sun3.png');
sun.fill = sunTexture;
sun.stroke='none';



let firstRgbSun=101;
let firstRgbSky=101;

let secondRgbSun=192;
let secondRgbSky=192;

let thirdRgbSun=251;
let thirdRgbSky=251;

function Draw(){

	let sunColor="rgb("+firstRgbSun+", "+ secondRgbSun+", "+thirdRgbSun+")";
	let SkyColor="rgb("+firstRgbSky+", "+secondRgbSky+", "+ thirdRgbSky+ ")";
	//Create top gradient

	let linearGradient = two.makeLinearGradient(
	two.width / 2, - two.height / 2,
	two.width / 2, two.height / 2,
	new Two.Stop(0.5,SkyColor,1),
	new Two.Stop(1, sunColor,1),

	);
	topHalf.fill = linearGradient;		//top


	topHalf.noStroke();
	}
	Draw();


bot.fill = 'rgb(55, 55, 55)';

bot.noStroke();

// Don't forget to tell two to render everything
// to the screen
two.update();

//Animating the sun
var sunGroup = two.makeGroup().add(sun);
sunGroup.translation.set(centerX,centerY);

two.bind("update", function(frameCount) {
	
////////////////////////////////////////////////////////////////////////////////////////////////////
	//BEGINNING OF CODE THAT SHOWS CURRENT TIME
	///////////////////////////////////////////////////////////////////////////////////////////////
	
	getCalcData();
	
	
////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	let sunPosition = sun.getBoundingClientRect();

	let sunCenter = {
		x : Math.floor(sunPosition.left + sunPosition.width / 2),
		y : Math.floor(sunPosition.top + sunPosition.height / 2)
	}
	
	let radius = Math.floor(Math.sqrt(Math.pow(Math.abs(sunCenter.y - centerY), 2) + Math.pow(Math.abs(sunCenter.x - centerX), 2)));
	
	
	let sideA = Math.floor(Math.abs(sunCenter.y - centerY));
	let sideB = Math.floor(Math.abs(sunCenter.x - centerX));	

	let C = (Math.pow(sideB, 2) + Math.pow(radius, 2) - Math.pow(sideA, 2)) / (2 * sideB * radius);
	let cAngle = Math.floor(Math.acos(C) * 180 / Math.PI);
	
	//cAngle is the angle between the Sun and the Earth. It is 0 at sunrise
	let start=parseInt(getCalcData().sunriseTime.split(":")[0])*60+parseInt(getCalcData().sunriseTime.split(":")[1]);
	let end=parseInt(getCalcData().sunsetTime.split(":")[0])*60+parseInt(getCalcData().sunsetTime.split(":")[1]);
	let minutes=end-start;
	let q=minutes/2;
	
	let DayMinutesPerDegree=q/90;
	let NightMinutesPerDegree=(24*60-minutes)/180;
	let minutesSinceStartQ1;
	
	//0 to 90 degrees 
	if(sunCenter.x>321&&sunCenter.y<250&&sunCenter.x<=500){
		if(cAngle!==cAngle){
			cAngle=90;			
		}
		
		let minutesSinceStartQ1=cAngle*DayMinutesPerDegree;
		let pMins=document.getElementById("minutes");
		let currentHour=Math.floor((start+minutesSinceStartQ1)/60);
		let currentMinutes=Math.floor((start+minutesSinceStartQ1)%60);
		if(Math.floor(currentMinutes)>0&&Math.floor(currentMinutes)<10){
			currentMinutes="0"+currentMinutes;
		}
		pMins.innerHTML="The current time is: "+currentHour+ ":"+currentMinutes+" .";
		
 	   if(firstRgbSky<101){
		   firstRgbSky=10+cAngle;
	   }
	   if(secondRgbSky<192){
		   secondRgbSky=cAngle*2;
	   }
	   if(thirdRgbSky<251){
		   thirdRgbSky=cAngle*2.5;
	   }
	   if(firstRgbSun>101){
	   firstRgbSun=204-cAngle*1.5;
	   }
	   if(secondRgbSun<192){
	   secondRgbSun=92+cAngle;
	   }
       if(thirdRgbSun<251){
       thirdRgbSun=10+2*cAngle;
       }
	    Draw();
		
	}
	
	//от 90 до 180 градуса
	if(sunCenter.x>500&&sunCenter.y<250){
		//console.log(cAngle)
		
		let minutesSinceStartQ1=(-cAngle+180)*DayMinutesPerDegree;
		let pMins=document.getElementById("minutes");
		let currentHour=Math.floor((start+minutesSinceStartQ1)/60);
		let currentMinutes=Math.floor((start+minutesSinceStartQ1)%60);
		if(Math.floor(currentMinutes)>0&&Math.floor(currentMinutes)<10){
			currentMinutes="0"+currentMinutes;
		}
		pMins.innerHTML="The current time is: "+currentHour+ ":"+currentMinutes+" ."; 
		
		firstRgbSky=cAngle*1.1;
		  secondRgbSky=2.1*cAngle;
	      thirdRgbSky=2.78*cAngle;
	      
	       firstRgbSun=-cAngle*1.1+191;
	       secondRgbSun=92+cAngle*1.05;
			thirdRgbSun=2.78*cAngle+10;

			
		  Draw();
		
	}
	//180 to 270 degrees
	if(sunCenter.x>500&&sunCenter.y>250){
		let minutesSinceStartQ1=minutes + cAngle*NightMinutesPerDegree;
		let pMins=document.getElementById("minutes");
		let currentHour=Math.floor((start+minutesSinceStartQ1)/60);
		let currentMinutes=Math.floor((start+minutesSinceStartQ1)%60);
			if(currentHour>=24){
	           currentHour=currentHour-24;
			}
		if(Math.floor(currentMinutes)>=0&&Math.floor(currentMinutes)<10){
			currentMinutes="0"+currentMinutes;
		}
		pMins.innerHTML="The current time is: "+currentHour+ ":"+currentMinutes+" ."; 
	}
	
	//270 to 360 degrees
	
	if(sunCenter.x<=500&&sunCenter.y>250){
		
		if(cAngle!==cAngle){
			cAngle=90;			
		}
		let minutesSinceStartQ1=minutes+ (180-cAngle)*NightMinutesPerDegree;
		let pMins=document.getElementById("minutes");
		let currentHour=Math.floor((start+minutesSinceStartQ1)/60);
		let currentMinutes=Math.floor((start+minutesSinceStartQ1)%60);
		if(currentHour>=24){
           currentHour=currentHour-24;
		}
		
		
		if(Math.floor(currentMinutes)>=0&&Math.floor(currentMinutes)<10){
			currentMinutes="0"+currentMinutes;
		}
		
		pMins.innerHTML="The current time is: "+currentHour+ ":"+currentMinutes+" .";
	
		
	}
	
	if(sunCenter.y>250){
    	firstRgbSun=204;
    	firstRgbSky=0;
    	secondRgbSun=92;
    	secondRgbSky=0;
    	thirdRgbSun=10;
    	thirdRgbSky=0;
    	Draw();
	}	
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////
	//END OF CODE THAT SHOWS CURRENT TIME
///////////////////////////////////////////////////////////////////////////////////////////////	
  })
  .play();


  
function toRadians (angle) {
	return angle * (Math.PI / 180);
}


function CheckAngle(angle){
    var newX;
    var newY;
    if(angle>=0 && angle<=90){
        newX = centerX+Math.sin(toRadians(-90-angle))*orbitRadius; //calculating new coordinates
        newY = centerY+Math.cos(toRadians(-90-angle))*orbitRadius;
    }
    else {
        newX = centerX+Math.sin(toRadians(180-(-90+angle)))*orbitRadius; //calculating new coordinates
        newY= centerY+Math.cos(toRadians(180+(-90+angle)))*orbitRadius;
    }
    
    return {
		x : newX,
		y : newY
		
	} 
	
}

console.log(degrees);

function CalculationDegreesPerMinute(startH, endH, selectedH, selectedMin){
    var degInMinute = ((endH - startH)*60)/180;
	var mins = selectedMin - prevMinutes; //calculating how many minutes is the change
	var degreesMin = mins*degInMinute;
    var degrees = ((((selectedH-startH)*60))/degInMinute) + degreesMin; 
    return degrees;
 }		


function PrintTime(){
	 getCalcData();
		let p=document.getElementById("times");
		p.innerHTML="On the selected date the sun will rise at "+ fixZero(getCalcData().sunriseTime)+" in the morning and will set at "+fixZero(getCalcData().sunsetTime)+" o'clock."+
		"It will be at it's highest point at "+fixZero(getCalcData().solarNoon)+".";

		return{
			sunrise: parseInt(getCalcData().sunriseTime.split(':')[0]),
			sunset: parseInt(getCalcData().sunsetTime.split(':')[0]),
			noon: parseInt(getCalcData().solarNoon.split(':')[0])
		}
	}

document.getElementById("date").oninput = () => {
	document.getElementById("times").innerHTML="";
	PrintTime();
}

document.getElementById("towns").addEventListener("change",function(){
	document.getElementById("times").innerHTML="";
	PrintTime();
})

function easterEgg() {
	var vid = document.querySelector(".fullscreen-bg");
	var b = document.querySelector(".troll");
	vid.style.opacity = 0;
	b.style.opacity = 1;
	var audio = document.getElementById("myAudio");
	audio.play();
	document.body.style.backgroundImage = "url('resource/backgr.jpg')";		
	var body = document.querySelector("body");
	body.addEventListener('click', function(){
		alert("YOU HAVE BEEN CHOSEN! EMBRACE RELIGION OR Jesus WILL NOT FIX YOUR PAGE! AMEN! (or just refresh, sinner.)");
	});	
}

function fixZero(timeString){
	let ClockpickerHours=timeString.split(":")[0];
	let ClockpickerMinutes=timeString.split(":")[1];
	if(ClockpickerMinutes>=0&&ClockpickerMinutes<10){
		ClockpickerMinutes="0"+ClockpickerMinutes;
		}
	return ClockpickerHours + ":"+ ClockpickerMinutes;
}
document.getElementById("single-input").value=fixZero(document.getElementById("single-input").value);

function getCalcData(){
	let myInputArr=document.getElementById("date").value.replace((/\//g),"-").split("-").reverse();
	let temp=myInputArr[1];
	myInputArr[1]=myInputArr[2];
	myInputArr[2]=temp;
	let inputDate=myInputArr.join("-");
	let townsSelect=document.getElementById("towns").value;
	let lat;
	let long;
	switch(townsSelect)
	{
	case "Sofia":
		lat=42.6977;
		long=23.3219;
		break;
	case "London":	
		lat=51.5074;
		long=0.1278;
		break;
	case "NewYork":
		lat=40.7128;
		long=74.0060;
		break;
	case "Sydney":	
		lat=33.8688;
		long=151.2093;
		break;
	case "CapeTown":	
		lat=33.9249;
		long=18.4241;
		break;

	}
	times= SunCalc.getTimes(new Date(inputDate), lat, long);

	return{
		
		sunriseTime:times.sunrise.getHours()+":"+times.sunrise.getMinutes(),
		sunsetTime:times.sunset.getHours()+":"+times.sunset.getMinutes(),
		solarNoon:times.solarNoon.getHours()+":"+times.solarNoon.getMinutes()
	}
}
