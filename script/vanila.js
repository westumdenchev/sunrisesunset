const CANVAS_SELECTOR = '.canvas';

class Drawable {
	
}

class SphericalObject extends Drawable {
	constructor() {
		super();
	}
	draw() {
		Stage.getContext().beginPath();
		Stage.getContext().arc(this.x, this.y, this.r, this.sAngle, this.eAngle);
		Stage.getContext().fillStyle = this.fillStyle;
		Stage.getContext().lineWidth = 0;
		Stage.getContext().fill();
	}
}

class Earth extends SphericalObject {
	constructor() {
		super();
		this.x = Stage.getStageCenter().x;
		this.y = Stage.getStageCenter().y;
		this.r = 100;
		this.sAngle = 0;
		this.eAngle = 2*Math.PI;
		this.fillStyle = "blue";
	}
}

class Sun extends SphericalObject {
	constructor() {
		super();

		this.x = Stage.getStageCenter().x + -100;
		this.y = Stage.getStageCenter().y + -100;
		this.r = 20;
		this.sAngle = 0;
		this.eAngle = 2*Math.PI;
		this.fillStyle = "orange";
	}

	doOrbit() {
		this.x += 1;
		this.y -= 1;
		this.draw();
	}
}

class Stage {
	constructor() {
		
		this.objects = [];
	}

	static getStageCenter() {
		return {
			x : 1000 / 2,
			y : 500 / 2
		}
	}

	static getContext() {
		var canvas = document.querySelector(CANVAS_SELECTOR);
		var ctx = canvas.getContext('2d');
		return ctx;
	}
	
	static clear() {
		Stage.getContext().clearRect(0, 0, 1000, 500);
	}

	addObject(object) {
		this.objects.push(object);

		object.draw();
	}
}

let stage = new Stage();

let earth = new Earth();
let sun = new Sun();




function draw() {

	Stage.clear();
	stage.addObject(earth);
	stage.addObject(sun);

	sun.doOrbit();

	window.requestAnimationFrame(draw);	
}

draw();